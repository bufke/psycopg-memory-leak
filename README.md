1. Start databases manually or via `docker compose up`
2. Install packages with poetry `poetry install` and migrate database
3. `celery -A memory_leak worker --concurrency 1`
4. Run many memory_leak.tasks.count_it tasks and watch memory go up

```bash
top -p `pgrep -d "," celery`
```

```python
from memory_leak.tasks import count_it
for _ in range(10000): count_it.delay()
```

# Theory

The leak only occurs when:

- Using psycopg-c (pure python version has no leak)
- Using `django.contrib.postgres` and HStore somewhere in a django app (The leak disappears when removing postgres from INSTALLED_APPS)
- The celery task must interact with Postgres (printing a message will not cause the leak for example)
