import gc
from collections import defaultdict
from django.contrib.auth.models import User
from celery import shared_task

@shared_task
def count_it():
    User.objects.count()


@shared_task
def show_objects():
    gc.collect()
    objs = gc.get_objects()

    c = defaultdict(int)
    for o in objs:
        c[type(o)] += 1

    for key, value in c.items():
        if value >= 2000:
            print(f"{value}:  {key}")

    print("---------------")
    c = defaultdict(int)
    for o in objs:
        c[type(o).__name__] += 1

    for key, value in c.items():
        if value >= 2000:
            print(f"{value}:  {key}")

